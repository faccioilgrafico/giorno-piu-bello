<title>Il giorno pi&ugrave; bello - Corriere.it</title>
<meta name="title" content="Il giorno pi&ugrave; bello">
<meta name="description" content="I messaggi dei lettori del Corriere della Sera">
<meta name="keywords" content="coronavirus, covid-19, lockdown, messaggi, lettori">
<meta property="fb:app_id" content="203568503078644">
<meta property="og:title" content="Il giorno pi&ugrave; bello">
<meta property="og:description" content="I messaggi dei lettori del Corriere della Sera">
<meta property="og:type" content="article">
<meta property="og:locale" content="it_IT">
<meta property="og:url" itemprop="url" content="<!--#echo var=url -->/">
<meta property="og:image" content="<!--#echo var=img -->/social.png">
<meta property="og:site_name" content="Corriere della Sera">
<meta name="author" content="Corriere.it">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@Corriere">
<meta name="twitter:creator" content="@Corriere">
<meta name="twitter:title" content="Il giorno pi&ugrave; bello">
<meta name="twitter:description" content="I messaggi dei lettori del Corriere della Sera">
<meta name="twitter:image" content="<!--#echo var=img -->/social.png">
<link rel="canonical" href="<!--#echo var=url -->/">
<meta property="vr:canonical" content="<!--#echo var=url -->/">

<meta name="robots" content="noindex, nofollow">
