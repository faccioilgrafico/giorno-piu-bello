var handlebars = require('handlebars');
var fs = require('fs');
var request = require("request");
var iconv = require('iconv-lite');

var url = 'https://www.corriere.it/infografiche/json-table/2020/giorno-piu-bello.json';

url+="?ie="+(new Date()).getTime();
var fooJson = {};


request({
    headers: {
      'User-Agent': 'request',
      'Cache-Control': 'private, no-cache, no-store, must-revalidate',
      'Expires': '-1',
      'Pragma': 'no-cache'
    },
    url: url,
    json: true
}, function (error, response, body) {
// test del push to deploy su ftp
    if (!error && response.statusCode === 200) {
        // console.log(body) // Print the json response
        fooJson = body;
        var voce = {};

function sortObject(obj, testVal) {
    var arr = [];
    var prop;
    
    for (prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            arr.push({
                'key': prop,
                'value': obj[prop][testVal]
            });
        }
    }
    arr.sort(function(a, b) {
        if (a.value > b.value) {
          return 1;
        }
        if (a.value < b.value) {
          return -1;
        }
        // a must be equal to b
        return 0;
    });
    return arr; // returns array
}
/*
        body.infografiche.forEach(function(infografiche) {
            for (var key in infografiche) {
              if ( voce[key] == undefined ) 
                    voce[key] = [];
                    voce[key].push( infografiche[key] );
            }
        });
*/
        body.infografiche.forEach(function(elem) {
            if(voce['infografiche'] == undefined)
                voce['infografiche'] = [];
            voce['infografiche'].push( elem );
        });


/* registro gli helpers di handlebars qui! (prima di richiamarli all'interno delle funzioni di generazione) */
handlebars.registerHelper("noSpaces", function(input) {
  if ( input == "" || input == undefined || input == null ){
    output = "";
    return output;
  } else {
    output = input.toString();
    // console.log(output.toLowerCase().replace(/'/g, "\-").replace(/\s+/g, '-').normalize() );
    return output.toLowerCase().replace(/<\/b>/g, "").replace(/<b>/g, "").replace(/<\/i>/g, "").replace(/<i>/g, "").replace(/\?/g, "").replace(/«/g, "").replace(/»/g, "").replace(/\//g, "-").replace(/\!/g, "").replace(/,/g, "").replace(/á/g, "a").replace(/à/g, "a").replace(/è/g, "e").replace(/é/g, "e").replace(/ò/g, "o").replace(/ù/g, "u").replace(/ì/g, "i").replace(/\!/g, "").replace(/:/g, "").replace(/\./g, "").replace(/\)/g, "").replace(/\(/g, "").replace(/%/g, "percento").replace(/’/g, "\-").replace(/'/g, "\-").replace(/\s+/g, '-');
  }
});



/* trasformo mesi in numeri */
handlebars.registerHelper("numeri", function(input) {
  if ( input == "gennaio"){
    output = "01";
    return output;
  } 
  if ( input == "febbraio"){
    output = "02";
    return output;
  } 
  if ( input == "marzo"){
    output = "03";
    return output;
  } 
  if ( input == "aprile"){
    output = "04";
    return output;
  }   
  if ( input == "maggio"){
    output = "05";
    return output;
  } 
  if ( input == "giugno"){
    output = "06";
    return output;
  } 
  if ( input == "luglio"){
    output = "07";
    return output;
  } 
  if ( input == "agosto"){
    output = "08";
    return output;
  }
  if ( input == "settembre"){
    output = "09";
    return output;
  } 
  if ( input == "ottobre"){
    output = "10";
    return output;
  } 
  if ( input == "novembre"){
    output = "11";
    return output;
  } 
  if ( input == "dicembre"){
    output = "12";
    return output;
  }   
  else {}
});

handlebars.registerHelper("normalizzami", function(input) {
  if ( input == "" || input == undefined || input == null ){
    output = "";
    return output;
  } else {
    // output = html_encode(input);
    input = input.replace(/[^\w ]/g, function(char) {
      return dict2[char] || char;
    });
    // input = iconv.encode(input, "UTF-8")
    return input;
    // output = htmlEncode(input);
    // return htmlDecode(output);
  }
});
handlebars.registerHelper("normalizzami-lower", function(input) {
  if ( input == "" || input == undefined || input == null ){
    output = "";
    return output;
  } else {
    // output = html_encode(input);
    input = input.toLowerCase().replace(/[^\w ]/g, function(char) {
      return dict2[char] || char;
    });
    // input = iconv.encode(input, "UTF-8")
    return input;
    // output = htmlEncode(input);
    // return htmlDecode(output);
  }
});

handlebars.registerHelper("normalizzamiNOTAG", function(input) {
  if ( input == "" || input == undefined || input == null ){
    output = "";
    return output;
  } else {
    // output = html_encode(input);
    input = input.replace(/<\/b>/g, "").replace(/<b>/g, "").replace(/<\/i>/g, "").replace(/<i>/g, "").replace(/[^\w ]/g, function(char) {
      return dict2[char] || char;
    });
    // input = iconv.encode(input, "UTF-8")
    return input;
    // output = htmlEncode(input);
    // return htmlDecode(output);
  }
});

handlebars.registerHelper('each_upto', function(ary, max, options) {
    if(!ary || ary.length == 0)
        return options.inverse(this);

    var result = [ ];
    for(var i = 0; i < max && i < ary.length; ++i)
        result.push(options.fn(ary[i]));
    return result.join('');
});

handlebars.registerHelper('reverse', function (arr) {
    arr.reverse();
});

handlebars.registerHelper("inc", function(value, options) {
    return parseInt(value) +1;
});

handlebars.registerHelper('iff', function(a, operator, b, opts) {
    var bool = false;
    switch(operator) {
       case '==':
           bool = a == b;
           break;
       case '>':
           bool = a > b;
           break;
       case '<':
           bool = a < b;
           break;
       default:
           throw "Unknown operator " + operator;
    }
 
    if (bool) {
        return opts.fn(this);
    } else {
        return opts.inverse(this);
    }
});

handlebars.registerHelper('compare', function(lvalue, rvalue, options) {
    if (arguments.length < 3)
        throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
    var operator = options.hash.operator || "==";
    var operators = {
        '==':       function(l,r) { return l == r; },
        '===':      function(l,r) { return l === r; },
        '!=':       function(l,r) { return l != r; },
        '<':        function(l,r) { return l < r; },
        '>':        function(l,r) { return l > r; },
        '<=':       function(l,r) { return l <= r; },
        '>=':       function(l,r) { return l >= r; },
        'typeof':   function(l,r) { return typeof l == r; }
    }
    if (!operators[operator])
        throw new Error("Handlerbars Helper 'compare' doesn't know the operator "+operator);
    var result = operators[operator](lvalue,rvalue);
    if( result ) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});

        // index
        fs.readFile('templates/voci.hbs', function(err, data){
          if (!err) {
            var source = data.toString();
            renderToString(source, voce, "voci.html");
          } else {
          }
        });
        // totali voci
        fs.readFile('templates/tot.hbs', function(err, data){
          if (!err) {
            var source = data.toString();
            renderToString(source, voce, "tot.inc");
          } else {
          }
        });
        /* dichiaro la funzione che applica i template e che viene richiamata ad ogni generazione di file e template */
        // this will be called after the file is read
        function renderToString(source, data, filename) {
          var template = handlebars.compile(source);
          var outputString = template(data);
          outputString = iconv.encode(outputString, "ISO-8859-1");
          


          function readFileSync_encoding(filename, encoding) {
              var content = fs.readFileSync(filename);
              return iconvlite.decode(content, encoding);
          }

          apri_hanldetemple = '';
          chiudi_hanldetemple = '';

          fs.writeFileSync(filename, "");
          fs.appendFileSync(filename, apri_hanldetemple);
          fs.appendFileSync(filename, outputString);
          fs.appendFileSync(filename, chiudi_hanldetemple);
        }

    }
});

/**
 * Encodes special html characters
 * @param string
 * @return {*}
 */
function html_encode(string) {
    var ret_val = '';
    for (var i = 0; i < string.length; i++) { 
        if (string.codePointAt(i) > 127) {
            ret_val += '&#' + string.codePointAt(i) + ';';
        } else {
            ret_val += string.charAt(i);
        }
    }
    return ret_val;
}

/* qui metto i dizionari che mi servono per convertire i caratteri speciali, 
occhio che nel template handlebars serve estrarre la variabile con 3 parentesi graffe per esportare l'html */
var dict = {
"á":"a",
"â":"a",
"ã":"a",
"ä":"a",
"ç":"c",
"è":"e",
"é":"e",
"ê":"e",
"ë":"e",
"ì":"i",
"í":"i",
"î":"i",
"ï":"i",
"ñ":"n",
"ò":"o",
"ó":"o",
"ô":"o",
"õ":"o",
"ö":"o",
"ù":"u",
"ú":"u",
"û":"u",
"ü":"u",
"ý":"y",
"ÿ":"y",
"À":"A",
"Á":"A",
"Â":"A",
"Ã":"A",
"Ä":"A",
"Ç":"C",
"È":"E",
"É":"E",
"Ê":"E",
"Ë":"E",
"Ì":"I",
"Í":"I",
"Î":"I",
"Ï":"I",
"Ñ":"N",
"Ò":"O",
"Ó":"O",
"Ô":"O",
"Õ":"O",
"Ö":"O",
"Ù":"U",
"Ú":"U",
"Û":"U",
"Ü":"U",
"à":"a",
"Ý":"Y"
}

var dict2 = {
"À":"&Agrave;",
"Á":"&Aacute;",
"Â":"&Acirc;",
"Ã":"&Atilde;",
"Ä":"&Auml;",
"Å":"&Aring;",
"Æ":"&AElig;",
"Ç":"&Ccedil;",
"È":"&Egrave;",
"É":"&Eacute;",
"Ê":"&Ecirc;",
"Ë":"&Euml;",
"Ì":"&Igrave;",
"Í":"&Iacute;",
"Î":"&Icirc;",
"Ï":"&Iuml;",
"Ð":"&ETH;",
"Ñ":"&Ntilde;",
"Ò":"&Ograve;",
"Ó":"&Oacute;",
"Ô":"&Ocirc;",
"Õ":"&Otilde;",
"Ö":"&Ouml;",
"×":"&times;",
"Ø":"&Oslash;",
"Ù":"&Ugrave;",
"Ú":"&Uacute;",
"Û":"&Ucirc;",
"Ü":"&Uuml;",
"Ý":"&Yacute;",
"Þ":"&THORN;",
"ß":"&szlig;",
"à":"&agrave;",
"á":"&aacute;",
"â":"&acirc;",
"ã":"&atilde;",
"ä":"&auml;",
"å":"&aring;",
"æ":"&aelig;",
"ç":"&ccedil;",
"è":"&egrave;",
"é":"&eacute;",
"ê":"&ecirc;",
"ë":"&euml;",
"ì":"&igrave;",
"í":"&iacute;",
"î":"&icirc;",
"ï":"&iuml;",
"ð":"&eth;",
"ñ":"&ntilde;",
"ò":"&ograve;",
"ó":"&oacute;",
"ô":"&ocirc;",
"õ":"&otilde;",
"ö":"&ouml;",
"÷":"&divide;",
"ø":"&oslash;",
"ù":"&ugrave;",
"ú":"&uacute;",
"û":"&ucirc;",
"ü":"&uuml;",
"ý":"&yacute;",
"þ":"&thorn;",
"ÿ":"&yuml;",
"¡":"&iexcl;",
"¢":"&cent;",
"£":"&pound;",
"¤":"&curren;",
"¥":"&yen;",
"¦":"&brvbar;",
"§":"&sect;",
"¨":"&uml;",
"©":"&copy;",
"ª":"&ordf;",
"«":"&laquo;",
"¬":"&not;",
"®":"&reg;",
"¯":"&macr;",
"°":"&deg;",
"±":"&plusmn;",
"²":"&sup2;",
"³":"&sup3;",
"´":"&acute;",
"µ":"&micro;",
"¶":"&para;",
"·":"&middot;",
"¸":"&cedil;",
"¹":"&sup1;",
"º":"&ordm;",
"»":"&raquo;",
"¼":"&frac14;",
"½":"&frac12;",
"¾":"&frac34;",
"€":"&euro;",
"‚":"&sbquo;",
"ƒ":"&fnof;",
"„":"&bdquo;",
"…":"&hellip;",
"†":"&dagger;",
"‡":"&Dagger;",
"ˆ":"&circ;",
"‰":"&permil;",
"Š":"&Scaron;",
"‹":"&lsaquo;",
"Œ":"&OElig;",
"Ž":"&Zcaron;",
"‘":"&lsquo;",
"’":"&rsquo;",
"“":"&ldquo;",
"”":"&rdquo;",
"•":"&bull;",
"–":"&ndash;",
"—":"&mdash;",
"˜":"&tilde;",
"™":"&trade;",
"š":"&scaron;",
"›":"&rsaquo;",
"œ":"&oelig;",
"ž":"&zcaron;",
"Ÿ":"&Yuml;",
"Ā":"&Amacr;",
"ā":"&amacr;",
"Ă":"&Abreve;",
"ă":"&abreve;",
"Ą":"&Aogon;",
"ą":"&aogon;",
"Ć":"&Cacute;",
"ć":"&cacute;",
"Ĉ":"&Ccirc;",
"ĉ":"&ccirc;",
"Ċ":"&Cdod;",
"ċ":"&cdot;",
"Č":"&Ccaron;",
"č":"&ccaron;",
"Ď":"&Dcaron;",
"ď":"&dcaron;",
"Đ":"&Dstrok;",
"đ":"&dstrok;",
"Ē":"&Emacr;",
"ē":"&emacr;",
"Ė":"&Edot;",
"ė":"&edot;",
"Ę":"&Eogon;",
"ę":"&eogon;",
"Ě":"&Ecaron;",
"ě":"&ecaron;",
"Ĝ":"&Gcirc;",
"ĝ":"&gcirc;",
"Ğ":"&Gbreve;",
"ğ":"&gbreve;",
"Ġ":"&Gdot;",
"ġ":"&gdot;",
"Ģ":"&Gcedil;",
"ģ":"&gcedil;",
"Ĥ":"&Hcirc;",
"ĥ":"&hcirc;",
"Ħ":"&Hstrok;",
"ħ":"&hstrok;",
"Ĩ":"&Itilde;",
"ĩ":"&itilde;",
"Ī":"&Imacr;",
"ī":"&imacr;",
"Į":"&Iogon;",
"į":"&iogon;",
"İ":"&Idot;",
"ı":"&inodot;",
"Ĳ":"&IJlog;",
"ĳ":"&ijlig;",
"Ĵ":"&Jcirc;",
"ĵ":"&jcirc;",
"Ķ":"&Kcedil;",
"ķ":"&kcedli;",
"ĸ":"&kgreen;",
"Ĺ":"&Lacute;",
"ĺ":"&lacute;",
"Ļ":"&Lcedil;",
"ļ":"&lcedil;",
"Ľ":"&Lcaron;",
"ľ":"&lcaron;",
"Ŀ":"&Lmodot;",
"ŀ":"&lmidot;",
"Ł":"&Lstrok;",
"ł":"&lstrok;",
"Ń":"&Nacute;",
"ń":"&nacute;",
"Ņ":"&Ncedil;",
"ņ":"&ncedil;",
"Ň":"&Ncaron;",
"ň":"&ncaron;",
"ŉ":"&napos;",
"Ŋ":"&ENG;",
"ŋ":"&eng;",
"Ō":"&Omacr;",
"ō":"&omacr;",
"Ő":"&Odblac;",
"ő":"&odblac;",
"Œ":"&OElig;",
"œ":"&oelig;",
"Ŕ":"&Racute;",
"ŕ":"&racute;",
"Ŗ":"&Rcedil;",
"ŗ":"&rcedil;",
"Ř":"&Rcaron;",
"ř":"&rcaron;",
"Ś":"&Sacute;",
"ś":"&sacute;",
"Ŝ":"&Scirc;",
"ŝ":"&scirc;",
"Ş":"&Scedil;",
"ş":"&scedil;",
"Š":"&Scaron;",
"š":"&scaron;",
"Ţ":"&Tcedil;",
"ţ":"&tcedil;",
"Ť":"&Tcaron;",
"ť":"&tcaron;",
"Ŧ":"&Tstrok;",
"ŧ":"&tstrok;",
"Ũ":"&Utilde;",
"ũ":"&utilde;",
"Ū":"&Umacr;",
"ū":"&umacr;",
"Ŭ":"&Ubreve;",
"ŭ":"&ubreve;",
"Ů":"&Uring;",
"ů":"&uring;",
"Ű":"&Udblac;",
"ű":"&udblac;",
"Ų":"&Uogon;",
"ų":"&uogon;",
"Ŵ":"&Wcirc;",
"ŵ":"&wcirc;",
"Ŷ":"&Ycirc;",
"ŷ":"&ycirc;",
"Ÿ":"&Yuml;",
"Ź":"&Zacute;",
"ź":"&zacute;",
"Ż":"&Zdot;",
"ż":"&zdot;",
"Ž":"&Zcaron;",
"ž":"&zcaron;",
"¿":"&iquest;"

}
